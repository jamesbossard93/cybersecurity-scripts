import subprocess

def get_mac_addresses():
    """
    Returns a list of MAC addresses of all devices on the local network.
    """
    # Run the arp command to get the MAC addresses
    output = subprocess.check_output(["arp", "-a"])

    # Split the output into lines and extract the MAC addresses
    lines = output.decode("utf-8").strip().split("\n")
    mac_addresses = []
    for line in lines:
        if ":" in line:
            mac_address = line.split()[3]
            mac_addresses.append(mac_address)

    return mac_addresses

if __name__ == "__main__":
    # Get the MAC addresses and print them to the console
    mac_addresses = get_mac_addresses()
    for mac_address in mac_addresses:
        print(mac_address)

    # Log the MAC addresses to a file
    with open("mac_addresses.log", "a") as f:
        for mac_address in mac_addresses:
            f.write(mac_address + "\n")
