import requests
import sys
import time

""" A simple URL enumerator which checks for certain pages.
    Requires user input of URL."""

URL = sys.argv[1]


potential_list = ['/robots.txt', '/passwords.txt']

url_enumeration = {}

for i in potential_list:
    time.sleep(1)
    # Do main loop here.
    page = requests.get(URL+i)
    # check if url is valid
    if '404' in str(page):
        continue
    else:
        # scrape page
        url_enumeration[i] = page.text
        


print(url_enumeration)